# docker nodeJS
nodeJS: 12.x LTS
npm: 5.x

## tools
 * npm
 * yarn
 * bower
 * grunt
 * gulp
 * sass
 * less


## features
 * node works as Your local user (on Linux, Mac, Windows)
 * node uses Your local SSH key
 * common cache directory for all tools `~/cache` is stored locally
 
 
## usage
```
docker run -it --rm -w /usr/src/app \
    -v $(pwd):/usr/src/app -v ~/.docker-node-cache:/home/node/cache -v ~/.ssh:/home/node/.ssh \
    amsdard/node:12 npm help
```


## Yake usage - https://yake.amsdard.io/
```
# tools
node: "docker run -it --rm -w /usr/src/app
      -v $(pwd):/usr/src/app -v ~/.docker-node-cache:/home/node/cache -v ~/.ssh:/home/node/.ssh
      amsdard/node:12 $CMD"

# aliases
npm: $BIN node npm $CMD
yarn: $BIN node yarn $CMD
bower: $BIN node bower $CMD
grunt: $BIN node grunt $CMD
gulp: $BIN node gulp $CMD
```


samples:
`yake yarn add jquery --dev`
